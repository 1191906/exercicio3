algoritmo "exercicio3"
ED
       Num1,Num2,Maior,Menor,Resultado :inteiros
Inicio
     escrever ("introduzir números:", Num1, Num2)
     ler (Num1, Num2)
        se(Num1>Num2)então
          Maior <-Num1
          Menor <-Num2
        fimse
        se(Num2>Num1)então
          Maior <-Num2
          Menor <-Num1
        fimse
     ler (Maior, Menor)
     Resultado <- (Maior - Menor)       
     Escrever("A Subtração é", Resultado)
Fim